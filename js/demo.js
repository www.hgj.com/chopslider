$(function() {
    var g = $(".cs3").html(),
        e = {
            effects: "tiles3d",
            autoplay: {
                enabled: !0,
                delay: 2E3
            },
            navigation: {
                enabled: !0,
                next: ".cs3-slide-next",
                prev: ".cs3-slide-prev"
            },
            pagination: {
                enabled: !0,
                container: ".cs3-pagination"
            }
        };
    $(".cs3").cs3(e);
});


