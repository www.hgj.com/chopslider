#chopslider
#demo示例:

html:

<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>demo chopSlider3</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="style/normalize.css">
    <link rel="stylesheet" href="style/idangerous.chopslider-3.4.css">
    <link rel="stylesheet" href="style/demo.css">
    <script src="js/modernizr-2.5.3.min.js"></script>
</head>

<body>
    <div class="cs3">
        <div class="cs3-slide"><img src="images/e-2.jpg">
        </div>
        <div class="cs3-slide"><img src="images/e-3.jpg">
        </div>
        <div class="cs3-slide"><img src="images/e-4.jpg">
        </div>
        <div class="cs3-slide"><img src="images/e-5.jpg">
        </div>
        <div class="cs3-slide"><img src="images/e-6.jpg">
        </div>
        <div class="cs3-slide"><img src="images/e-7.jpg">
        </div>
        <a href="#" class="cs3-slide-next"></a>
        <a href="#" class="cs3-slide-prev"></a>
        <div class="cs3-pagination"></div>
    </div>

    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    <script src="js/chopslider.js"></script>
    <script src="js/demo.js"></script>

</body>

</html>










css:
html,body {
    background: #fff;
    font-family: Arial, Helvetica, sans-serif;
    font-size: 13px;
    margin: 0;
    padding: 0;
    position: relative;
    height: 100%;
    width: 100%;
    line-height: 1.4em;
    color: #444;
}
.cs3 {
    width: 600px;
    height: 300px;
    position: absolute;
    z-index: 10;
    margin: auto;
    right: 0;
    bottom: 0;
    left: 0;
    top: 0;
}
.cs3-slide-prev {
    background: url(skins/wood-nav.png) no-repeat left top;
    width: 20px;
    height: 33px;
    position: absolute;
    z-index: 11;
    left: 10px;
    top: 140px;
}
.cs3-slide-next {
    background: url(skins/wood-nav.png) no-repeat left bottom;
    width: 20px;
    height: 33px;
    position: absolute;
    z-index: 11;
    right: 10px;
    top: 140px;
}
.cs3-pagination {
    position: absolute;
    text-align: center;
    bottom: 20px;
    left: 50%;
    margin-left: -50px;
    background-color: rgba(255,255,255,0.4);
}
.cs3-pagination-switch {
    width: 10px;
    height: 10px;
    background: url(skins/whitepane-pag.png) no-repeat left top;
    display: inline-block;
    margin: 0 5px;
}
.cs3-active-switch {
    background: url(skins/whitepane-pag.png) no-repeat left bottom;
}





js:
$(function() {
    var g = $(".cs3").html(),
        e = {
            effects: "tiles3d",
            autoplay: {
                enabled: !0,
                delay: 2E3
            },
            navigation: {
                enabled: !0,
                next: ".cs3-slide-next",
                prev: ".cs3-slide-prev"
            },
            pagination: {
                enabled: !0,
                container: ".cs3-pagination"
            }
        };
    $(".cs3").cs3(e);
});




